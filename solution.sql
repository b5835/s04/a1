-- a
SELECT * FROM artists WHERE name LIKE "%d%";

-- b
SELECT * FROM songs WHERE length < 230;

-- c
SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;


-- d
SELECT * FROM albums LEFT OUTER JOIN artists ON artist_id = artists.id WHERE album_title LIKE "%a%";

-- e
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- f
SELECT * FROM albums LEFT JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;


-- Stretch Goals
-- Count the albums created by each artist
SELECT artists.id, name as artist, COUNT(artist_id) AS album_count FROM albums JOIN artists ON artist_id=artists.id GROUP BY artist_id;

-- Display the artist with more than 1 albums created
SELECT artists.id, name as artist, COUNT(artist_id) AS album_count FROM albums JOIN artists ON artist_id=artists.id GROUP BY artist_id HAVING COUNT(artist_id)>1 ORDER BY COUNT(artist_id) DESC;

-- Case 
SELECT artists.id, name AS artist, COUNT(artist_id) AS album_count, 
CASE 
WHEN COUNT(artist_id) > 10 THEN 'Senior Level'
WHEN COUNT(artist_id) > 1 THEN 'Juniour Level'
ELSE 'Entry Level'
END AS remarks
FROM albums JOIN artists ON artist_id=artists.id GROUP BY artist_id;

--Stored Procedure
DELIMITER //
CREATE PROCEDURE GenerateRemarksByAlbumCount()
BEGIN
SELECT artists.id, name AS artist, COUNT(artist_id) AS album_count, 
CASE 
WHEN COUNT(artist_id) > 10 THEN 'Senior Level'
WHEN COUNT(artist_id) > 1 THEN 'Juniour Level'
ELSE 'Entry Level'
END AS remarks
FROM albums JOIN artists ON artist_id=artists.id GROUP BY artist_id;
END//


-- a
DELIMITER //
CREATE PROCEDURE searchArtist(IN search VARCHAR(50))
BEGIN
SELECT * FROM artists WHERE name LIKE ("%" + search + "%");
END//

-- b
DELIMITER //
CREATE PROCEDURE printSong(IN len TIME)
BEGIN
SELECT * FROM songs WHERE length < len;
END //

